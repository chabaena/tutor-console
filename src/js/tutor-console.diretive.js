	angular.module('tutorConsole')
		.directive('tcConsole', tcConsoleDirective);

	function tcConsoleDirective() {
		var directive = {
			restrict: 'E',
			replace: true,
			link: link
		};
		return directive;

		function link(scope, element, attrs) {
      var options = scope.$eval(attrs.options);
      var tc = scope.$eval(attrs.console);
      if(!tc instanceof UITutorConsole)
        throw new TypeError('The attribute "console" must be a instance of'
          + ' UITutorConsole.');
      tc.render(element.parent(), options);
		}
	}
