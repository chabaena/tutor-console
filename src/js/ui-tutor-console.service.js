	angular.module('tutorConsole')
		.service('UITutorConsole',['options','ES','EN','TutorConsole',UITutorConsole]);

	function UITutorConsole (options, ES, EN, TutorConsole) {
		var vm = this;
		vm.setOptions = setOptions;
		vm.getOptions = getOptions;
		vm.getForm  = getForm;
		vm.getInput  = getInput;
		vm.getContent = getContent;
 		vm.getPrompt  = getPrompt;
		vm.render = render;
		vm.updateContent = updateContent;
		vm.reset = reset;
		vm.hide = hide;
		vm.clearConsole = clearConsole;
		vm.execCommand = execCommand;

		activate();

		return vm;
		
		var _parentElement = null;
		var _echo = true;
		var _msgLang = {};

		function activate() {
		}

		function defaultCommand() { // Default command
			return {
				':print-console': function(_this) {
					var th = _this instanceof Array? _this[0]: _this;
					var source = th.getContent();
					var imp = window.open('', _msgLang["PRINT_TITLE"]);
					imp.document.write('<html><head>'
						+ '<link href="css/bootstrap.min.css" rel="stylesheet">'
						+ '<link href="css/tide.css" rel="stylesheet">'
						+ '</head><body>' + $(source).html() + '</body>');
					imp.document.close();
					imp.print();
					imp.close();
					return " ";
				},
				':clear': vm.clearConsole,
				':ilist': function(_this) {
					var th = _this instanceof Array? _this[0]: _this;
					var _interps = th.getOptions().interpreters;
					if(_interps === undefined || _interps.length === 0) {
						return _msgLang["NO_INTERPS"];
					} else {
						return _interps.join('</br>');
					}
				},
				':ishow': function(_this) {
					var th = _this instanceof Array? _this.shift(): _this;
					return _msgLang["CURRENT_INTERP"].
						replace('@1', th.getOptions().currentInterpreter);
				},
				':iset': function(tokens) {
					var th = tokens instanceof Array? tokens.shift(): tokens;
					if(tokens === undefined || tokens.length === 0)
						return _msgLang["NO_PARAM_INTERP"];
					var interp = tokens.shift();
					if(!th.getOptions().interpreters.includes(interp))
						return _msgLang["NOT_FOUND_INTERP"].replace('@1', interp);
					th.setOptions({ currentInterpreter: interp });
					return _msgLang["CHANGE_INTERP"].
							replace('@1', th.getOptions().currentInterpreter);
				}
			};
		}
		/**
 		 * Renders the DOM elements necessary for the management of the console
 		 * @param parentElement the DOM element parent
 		 * @param opts configuration options
 		 **/
		function render(parentElement, opts) {
			_parentElement = parentElement;
			_echo = true;
			// Sets the options
     	if(opts.commandConsole !== undefined){
        $.extend(true, opts.commandConsole, defaultCommand());
      } else {
        opts.commandConsole = defaultCommand();
      }
      TutorConsole.setOptions(opts);
			// setting language
			changeLanguage();
			// Render template
			_parentElement.html( 
				'<div class="' + vm.getOptions().cssClass.content
				+ '" id="tcContent"></div>'
				+ '<div><form id="tcForm">'
				+ '<span class="' + vm.getOptions().cssClass.prompt
				+ '" id="tcPrompt">' + vm.getOptions().prompt + '&nbsp;</span>'
				+ '<label for="tcCmdLine" style="display:none;"></label>'
				+ '<input class="' + vm.getOptions().cssClass.cmdLine
				+ '" id="tcCmdLine" type="text">'
				+ '</form></div>');
    	_parentElement.addClass(vm.getOptions().cssClass.parent);
    	_parentElement.click( function() { $('#tcCmdLine').focus(); });
			// 
			vm.getForm().on('submit', function( e ) {
        submitForm(e);
        return false;
      });
      vm.getInput().on('keydown', function(e) {
        inputKeydown(e);
      });
		}
		/**
		 * Gets the options
		 **/
		function getOptions() {
			return TutorConsole.getOptions();
		}
		/**
		 * Set the new options
		 * @param opts the new options
		 **/
		function setOptions( opts ){
			if(opts.commandConsole !== undefined){
				$.extend(true, opts.commandConsole, defaultCommand());
			}
			TutorConsole.setOptions(opts);
			if(opts.prompt !== undefined) {
				vm.getPrompt().html(opts.prompt + '&nbsp;');
				vm.clearConsole(vm);
			}
			if(opts.language !== undefined) changeLanguage();
		}
		/**
		 * Gets the form DOM element
		 **/
		function getForm() {
			return angular.element('#tcForm');
		}
		/**
		 * Gets the input DOM element
		 **/
		function getInput() {
			return angular.element('#tcCmdLine');
		}
		/**
		 * Gets the content DOM element 
		 **/
		function getContent() {
			return angular.element('#tcContent');
		}
		/**
		 * Gets the prompt DOM element 
		 **/
		function getPrompt() {
			return angular.element('#tcPrompt');
		}
		/**
		 * Update the content element with the result of the execution of a
		 * command
		 * @param cmdLine command line
		 * @data result for execution of command line 
		 **/
		function updateContent(cmdLine, data) {
			let echo = '<span class="' + vm.getOptions().cssClass.prompt + '">'
				+ vm.getOptions().prompt + '&nbsp;</span>'
				+ '<span>' + cmdLine + '</span>';
			let html = '<div>'
				+ (_echo ? echo : "")
				+ '<div>' + ((data)? data: '') + '</div>'
				+ '</div>';
			vm.getContent().append(html);
			vm.reset();
		}
		/**
		 *
		 **/
		function reset() {
			var input = vm.getInput();
			input.prop('disabled', false);
			input.val('');
			input.focus();
			var pp = $(_parentElement).parent();
			if(pp !== null || pp !== undefined)
				$(pp).scrollTop($(pp)[0].scrollHeight);
			else
				$(_parentElement).scrollTop($(_parentElement)[0].scrollHeight);
		}
		/**
		 *
		 **/
		function hide() {
			vm.getInput().prop('disabled', true);
		}
		/**
		 * Clean console
		 **/
		function clearConsole(_this) {
			var th = _this instanceof Array? _this[0]: _this;
			th.getContent().html('');
			th.reset();
		}
		/**
		 * Execute the command typed in the console
		 **/
		function submitForm(event ) {
			_echo = true;
			var value = vm.getInput().val();
			var tokens = value.split(/\s+/);
			var cmd = tokens[0];

			if(!value) return;

			vm.hide();

			vm.execCommand(cmd, tokens, true);
  	}
		/**
		 * Execute command
		 * @param cmd command to execute
		 * @param tokens array of params
		 * @param echo true prints the executed prompt, false ignores it
		 **/
		function execCommand(cmd, tokens, echo) {
			_echo = echo === undefined ? true : echo;
			let t = cmd.startsWith(':') ? tokens.filter((e) => e !== cmd) : tokens;
			let value = cmd + " " + t.join(', ');

			//Execute command
			let result = undefined;
			if(Object.keys(defaultCommand()).includes(cmd)) {
				// if it is a default command, we passed the command and a reference to
				// the class
				t.unshift(vm);
				result = TutorConsole.execCommand(cmd, t);
			} else {
				result = TutorConsole.execCommand(cmd, t);
			}
			if(result === false) {
				let msg = _msgLang["NOT_FOUND_CMD"].replace('@1', cmd);
				vm.updateContent(value, msg);
				return;
			}

			if(result == undefined) return;

			if(cmd.indexOf('clear') != -1 || !result)
				return;

    	// Show result into content
    	vm.updateContent(value, result);
		}
		/**
		 *
		 **/
		function inputKeydown( e ) {
			var keycode = e.keyCode;
			var opts = vm.getOptions();
			switch(keycode) {
				case 9:  // Tab: command completion
					e.preventDefault();
					var cmds = [];
					var value = vm.getInput().val();
					if(value.match( /^[^\s]{0,}$/ ) ) {
						var keys = Object.keys(opts.commandConsole);
						for(var i in keys){
							if(value.trim() === '' || keys[i].indexOf(value) === 0)
								cmds.push(keys[i]);
						}
						if(opts.commandAllowed !== undefined) {
							var list = opts.commandAllowed.filter((e) =>
								!opts.commandNotAllowed.includes(e));
							for(var i in list)
								if(value.trim() === '' || i.indexOf(value) === 0)
									cmds.push(list[i]);
						}
						var keys = Object.keys(opts.macros);
						for(var i in keys){
							if(value.trim() === '' || keys[i].indexOf(value) === 0)
								cmds.push(keys[i]);
						}
					}
					if(cmds.length > 1) {
						vm.updateContent(value, cmds.join('</br>'));
						vm.getInput().val(value);
					} else if(cmds.length === 1)
						vm.getInput().val(cmds.pop() + ' ');
					else
						vm.getInput().val(value);
					break;
				case 38: // Up arrow
				case 40: // Down arrow
					e.preventDefault();
					var res = TutorConsole.showCommandHistory(keycode===38? true: false);
					vm.getInput().val(res? res: '');
					break;
				default:
					break;
			}
		}
		/**
 		 *
 		 **/
		function changeLanguage() {
			if(vm.getOptions().language === "es") _msgLang = ES
			else _msgLang = EN
		}
	}
