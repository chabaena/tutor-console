(function() {
	'use strict'
	
	angular.module('tutorConsole', []);

	angular.module('tutorConsole')
		.constant('ES', {
			 "PRINT_TITLE": "Imprimir contenido de la consola"
			,"NO_INTERPS": "No hay intérpretes definidos"
			,"NO_PARAM_INTERP": "No se ha indicado intérprete. Uso:</br><pre>:iset intérprete</pre>"
			,"NOT_FOUND_INTERP": "El intérprete '@1' no está definido"
			,"CHANGE_INTERP": "Se ha cambido el intérprete a '@1'"
			,"NOT_FOUND_CMD": "@1 comando no encontrado"
			,"CURRENT_INTERP": "Interprete actual '@1'"
		});
	angular.module('tutorConsole')
		.constant('EN', {
			 "PRINT_TITLE": "Print console content"
			,"NO_INTERPS": "There are no defined interpretrs"
			,"NO_PARAM_INTERP": "Interpreter has not been indicated. Use:</br><pre>:iset interpreter</pre>"
			,"NOT_FOUND_INTERP": "The interpeter '@1' is not defined"
			,"CHANGE_INTERP": "The interpreter has been changed to '@1'"
			,"NOT_FOUND_CMD": "'@1' command not found"
			,"CURRENT_INTERP": "Current interpreter '@1'"
		});

	angular.module('tutorConsole')
		.value('options', {
      /* String shown in a command line to indicate it is waiting for orders */
      // Prompt configurable 
      prompt: '$:',
      /* Description runtime environment */
      banner: 'TutorConsole (v@version)',
      /* Styles that will be applied to the elements of the console */
      cssClass: {content: "", prompt: "", cmdLine: "", parent: ""},
      /* Allow list interpreters in the runtime environment */
      interpreters: [],
      /* */
      currentInterpreter: undefined,
      /* Allow list commands in the runtime environment
       * Format: [':cmd1', ':cmd2', ...] */
      commandAllowed: [],
      /* List of not allowed commands in the runtime environmet
       * Format: [':cmd1', ':cmd2', ...] */
      commandNotAllowed: [],
      /* Allow list macros in the runtime environment.
       * Format: {':macro-name': 'string-macro', ....}*/
      macros: {},
      /* List console commands permissible. This list will not be executed in
       * the runtime environment. 
       * Format: {':name-cmd': function(){}, ...}*/
      commandConsole: {},
      /* Function that allows the execution of a command in the runtime
       * environment.
       * Format: function(currentInterpreter, cmd, tokens) 
       * If the function returns "null" it will not show anything in console,
       * otherwise the value returned by the function will be showed.
       */
      fnExecCommand: undefined,
      /* Is command history enabled */
      history: true,
      /* Number of entries to be stored in history */
      historyEntries: 100,
			/* messages language */
			language: 'en'
    });
})();
