	angular.module('tutorConsole')
		.service('TutorConsole', ['options', TutorConsole]);

	function TutorConsole(options) {
		var vm = this;
		vm.setOptions = setOptions;
		vm.getOptions = getOptions;
		vm.execCommand = execCommand;
		vm.showCommandHistory = showCommandHistory;

		activate();

		return vm;

		var hcurrent;
		var history;

		function activate() {
			hcurrent = null;
		 	history = [];
		}

		/**
 		 * Sets the console options
 		 * @param opts options of configuration
 		 **/
		function setOptions(opts) {
			if(!options) return;
		 	var opKeys = Object.keys(opts);
    	if(opKeys.length === 0) return;
    	for(var i=0; i<opKeys.length; i++){
      	options[opKeys[i]] = opts[opKeys[i]];
    	}
		}
		/**
 		 * Gets the console options
 		 **/
		function getOptions() {
			return options;
		}
		/**
 		 * Execute command
 		 * @param cmd command to execute
 		 * @param tokens params
 		 **/
		function execCommand(cmd, tokens) {
			// Validate command to execute
			if(!isValidCommand(cmd))
				return false;

			// Save in the history list
			if(vm.getOptions().history) {
				if(history.length > vm.getOptions().historyEntries)
					history.shift();
				history.push( cmd + ' ' +
					tokens.filter(function(el) {
						return el instanceof UITutorConsole? false: 
							el.trim() === cmd.trim()? false: true;
					}).join(' '));
				hcurrent = null;
			}

			// If is console command, executing the associated function and the
			// return value is returned by function.    
			if(Object.keys(vm.getOptions().commandConsole).includes(cmd)){
				var _fn = vm.getOptions().commandConsole[cmd];
				if(_fn === undefined)
					return undefined;
				return _fn(tokens);
			}

			// If the command is a macro, we substitute the value and execute
			// the command
			if(Object.keys(vm.getOptions().macros).includes(cmd)){
				var expanded = vm.getOptions().macros[cmd];
				for(var i=0; i<tokens.length; i++)
					expanded = expanded.replace('{'+i+'}', tokens[i]);
				tokens = expanded.split(/\s+/);
				cmd = tokens[0]
				tokens = tokens.filter((e)=>tokens.indexOf(e)>0);
			}

			if(!vm.getOptions().fnExecCommand)
				return false;

			return vm.getOptions().fnExecCommand(vm.getOptions().currentInterpreter,
				cmd, tokens);
		}
		/**
		 * Shows the history of executed commands
		 * @param isUp the arrow up key has been pressed
		 **/
		function showCommandHistory(isUp) {
			if(!history) return;
			var len = history.length;
			if(isUp){ // up arrow
				if(hcurrent === null) hcurrent = len;
				if(hcurrent < 0) return;
				hcurrent--;
			} else { // down arrow
				if(hcurrent === null || hcurrent === (len - 1)) return;
				hcurrent++;
			}
			return hcurrent >= 0 || hcurrent <= len?
				history[hcurrent]: null;
		}
		/**
		 *
		 **/
		function isValidCommand(cmd) {
			var pattern = /^:(.*)/

			if(!pattern.test(cmd)) return true;
			var match = cmd.match(pattern);

			// Validate commandConsole
			if(Object.keys(vm.getOptions().commandConsole).includes(match[0]))
				return true;
			// Validate commandNotAllowed
			if(vm.getOptions().commandNotAllowed && vm.getOptions().commandNotAllowed.includes(match[0]))
				return false;
			// Validate commandAllowed
			if(vm.getOptions().commandAllowed && vm.getOptions().commandAllowed.includes(match[0]))
				return true;
			// Validate macros
			if(Object.keys(vm.getOptions().macros).includes(match[0]))
				return true;
			return false;
		}
	}
