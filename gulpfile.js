'use strict';

let gulp = require('gulp'),
		concat = require('gulp-concat'),
		replace = require('gulp-replace'),
		banner = require('gulp-banner'),
		minify = require('gulp-minify'),
		pkg = require('./package.json'),
		header = '/**\n' 
			+ ' * <%= pkg.name %> v<%=pkg.version %>\n' 
			+ ' * <%= pkg.description %>\n' 
			+ ' *\n'
			+ ' * Authors: <%= pkg.author %>\n'
			+ ' *     Url: <%= pkg.homepage %>\n'
			+ ' * Released under the <%= pkg.license %> license.\n'
			+ ' **/\n\n';
gulp.task('default', ['angular/build']);

gulp.task('angular/build',
	function () {
    gulp
      .src([ './src/js/tutor-console.module.js'
						,'./src/js/ui-tutor-console.service.js'
						,'./src/js/tutor-console.service.js'
						,'./src/js/tutor-console.diretive.js' ])
			.pipe(concat('angular-tutor-console.js'))
			.pipe(banner(header, {pkg: pkg}))
			.pipe(replace('@version', pkg.version))
      .pipe(gulp.dest('./dist/'));
		gulp
			.src(['./dist/angular-tutor-console.js'])
			.pipe(minify())
			.pipe(gulp.dest('./dist/'));
	}
);
