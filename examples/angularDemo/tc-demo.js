(function() {
	'use strict';

	angular.module("tc-demo", ["tutorConsole"]);

	angular.module("tc-demo").
		controller('DemoCtrl', ['$scope', 'UITutorConsole', DemoCtrl]);

	function DemoCtrl($scope, UITutorConsole) {
		var vm = this;
		vm.console = UITutorConsole;
		vm.options = {};

		activate();
		
		function activate() {
			vm.options = { prompt: '$>:',
				banner: 'Tutor Demo Console',
				commandConsole: {
					':hello': function( token ) {
						return '<pre>Hello, how are you ' + token.join(',') + '</pre>';
					}
				},
				macros: {':hi': ':hello {0}'},
				currentInterpreter: "Execute: ",
				fnExecCommand: function(interpreter, cmd, tokens) {
					return interpreter + " " + cmd + " " + tokens.join(',');
				}
			};
		}
	}
		
})();
