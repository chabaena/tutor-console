# tutor-console 

**Tutor-console** is a general purpose console emulator for angularJS applications.

Through the configuration options (see [Options](#options)) you can define:
- A list of interpreters execution, choosing one with the current interpreter on which the commands will be executed.
- A list of console commands, which allows us to execute functions independently of the interpreter (see [Command Console](#command-console)). 
- A list of macros.
- A command execution function in the current interpreter. It also provides two options that allow you to define command allowed and not allowed.


## Instalation

**Bower**

```shell
bower install tutor-console
```
## Usage
Add to your html file;
```html
<html ng-app="myApp">
  <head>
    ....
	<script src="<path-bower-components>/tutor-console/dist/angular-tutor-console.js"></script>
  </head>
  <body ng-controller="myCtrl as ctrl">
    ....
    <div id="divParent">
	<tc-console console='ctrl.console' options='ctrl.options'/>    
    </div>
    ....
  </body>
</html>
```

Add dependency to your application module:

```javascript
angular.module('myApp', ['tutor-console', ...]);
```

Inject `UITutorConsole` to your controller and create two scope variables as show bellow:

```javascript
(function() {
	'use strict';
    angular.module('myApp').
    	controller('myCtrl', ['UITutorConsole', myCtrl]);
    function myCtrl (UITutorConsole) {
    	var vm = this;
        ...
        vm.console = UITutorConsole;
        vm.options = {};
        ...
    }
})();
```
The options that can be configured are detailed in [Options](#options)

In the `example` folder you have a demo of the operation of this module.

## Options

- **prompt**: string shown in a command line to indicate it is waiting for orders. Default value: `$:`.
- **banner**: description runtime environment
- **cssClass**: styles that will be applied to the elements of the console. Format: `{content: "", prompt: "", cmdLine: "", parent: ""}`.
- **interpreters**: allow list interpreters in the runtime environment. Default value `[]`.
- **currentInterpreter**: current interpreter. Default value: `undefined`.
- **commandAllowed**: allow list commands in the runtime environment. Format: `[':cmd1', ':cmd2', ...]`.
- **commandNotAllowed:**: list of not allowed commands in the runtime environmet. Format: `[':cmd1', ':cmd2', ...]`.
- **macros**: allow list macros in the runtime environment. Format: `{':macro-name': 'string-macro', ....}`.
- **commandConsole**: list console commands permissible. This list will not be executed in the runtime environment. Format: `{':name-cmd': function(){}, ...}`.
- **fnExecCommand**: function that allows the execution of a command in the runtime environment. Format: `function(currentInterpreter, cmd, tokens)`. If the function returns "null" it will not show anything in console, otherwise the value returned by the function will be showed.
- **history**: is command history enabled. Default value: `true`.
- **historyEntries**: number of entries to be stored in history. Default value: `100`.
- **language**: Language in which to show messages. Default value `en`; registered languages: `es` y `en`. 

Example:
```javascript
	vm.options = { 
		prompt: '$>:',
		banner: 'Tutor Demo Console',
		commandConsole: {
			':hello': function( token ) {
				return '<pre>Hello, how are you ' + token.join(',') + '</pre>';
			}
		},
		macros: {':hi': ':hello {0}'},
		currentInterpreter: "Execute: ",
		fnExecCommand: function(interpreter, cmd, tokens) {
			return interpreter + " " + cmd + " " + tokens.join(',');
		}
	};
```
## Command console

There is a list of commands includes in the console:
- **:clear**, clear the console screen.
- **:print-console**, print the contents of the console.
- **:ishow**, show the current interpreter.
- **:ilist**, show the list of interpreters.
- **:iset**, set the current interpreter. The interpreter must be one of those contained int the list.
Usage: `:iset [interpreter-name]`

