/**
 * tutor-console v0.0.18
 * Allows execution of commands configured in different interpreters
 *
 * Authors: Abandos
 *     Url: https://github.com/Abandos/tutor-console
 * Released under the GPL-3.0 license.
 **/

(function() {
	'use strict'
	
	angular.module('tutorConsole', []);

	angular.module('tutorConsole')
		.constant('ES', {
			 "PRINT_TITLE": "Imprimir contenido de la consola"
			,"NO_INTERPS": "No hay intérpretes definidos"
			,"NO_PARAM_INTERP": "No se ha indicado intérprete. Uso:</br><pre>:iset intérprete</pre>"
			,"NOT_FOUND_INTERP": "El intérprete '@1' no está definido"
			,"CHANGE_INTERP": "Se ha cambido el intérprete a '@1'"
			,"NOT_FOUND_CMD": "@1 comando no encontrado"
			,"CURRENT_INTERP": "Interprete actual '@1'"
		});
	angular.module('tutorConsole')
		.constant('EN', {
			 "PRINT_TITLE": "Print console content"
			,"NO_INTERPS": "There are no defined interpretrs"
			,"NO_PARAM_INTERP": "Interpreter has not been indicated. Use:</br><pre>:iset interpreter</pre>"
			,"NOT_FOUND_INTERP": "The interpeter '@1' is not defined"
			,"CHANGE_INTERP": "The interpreter has been changed to '@1'"
			,"NOT_FOUND_CMD": "'@1' command not found"
			,"CURRENT_INTERP": "Current interpreter '@1'"
		});

	angular.module('tutorConsole')
		.value('options', {
      /* String shown in a command line to indicate it is waiting for orders */
      // Prompt configurable 
      prompt: '$:',
      /* Description runtime environment */
      banner: 'TutorConsole (v0.0.18)',
      /* Styles that will be applied to the elements of the console */
      cssClass: {content: "", prompt: "", cmdLine: "", parent: ""},
      /* Allow list interpreters in the runtime environment */
      interpreters: [],
      /* */
      currentInterpreter: undefined,
      /* Allow list commands in the runtime environment
       * Format: [':cmd1', ':cmd2', ...] */
      commandAllowed: [],
      /* List of not allowed commands in the runtime environmet
       * Format: [':cmd1', ':cmd2', ...] */
      commandNotAllowed: [],
      /* Allow list macros in the runtime environment.
       * Format: {':macro-name': 'string-macro', ....}*/
      macros: {},
      /* List console commands permissible. This list will not be executed in
       * the runtime environment. 
       * Format: {':name-cmd': function(){}, ...}*/
      commandConsole: {},
      /* Function that allows the execution of a command in the runtime
       * environment.
       * Format: function(currentInterpreter, cmd, tokens) 
       * If the function returns "null" it will not show anything in console,
       * otherwise the value returned by the function will be showed.
       */
      fnExecCommand: undefined,
      /* Is command history enabled */
      history: true,
      /* Number of entries to be stored in history */
      historyEntries: 100,
			/* messages language */
			language: 'en'
    });
})();

	angular.module('tutorConsole')
		.service('UITutorConsole',['options','ES','EN','TutorConsole',UITutorConsole]);

	function UITutorConsole (options, ES, EN, TutorConsole) {
		var vm = this;
		vm.setOptions = setOptions;
		vm.getOptions = getOptions;
		vm.getForm  = getForm;
		vm.getInput  = getInput;
		vm.getContent = getContent;
 		vm.getPrompt  = getPrompt;
		vm.render = render;
		vm.updateContent = updateContent;
		vm.reset = reset;
		vm.hide = hide;
		vm.clearConsole = clearConsole;
		vm.execCommand = execCommand;

		activate();

		return vm;
		
		var _parentElement = null;
		var _echo = true;
		var _msgLang = {};

		function activate() {
		}

		function defaultCommand() { // Default command
			return {
				':print-console': function(_this) {
					var th = _this instanceof Array? _this[0]: _this;
					var source = th.getContent();
					var imp = window.open('', _msgLang["PRINT_TITLE"]);
					imp.document.write('<html><head>'
						+ '<link href="css/bootstrap.min.css" rel="stylesheet">'
						+ '<link href="css/tide.css" rel="stylesheet">'
						+ '</head><body>' + $(source).html() + '</body>');
					imp.document.close();
					imp.print();
					imp.close();
					return " ";
				},
				':clear': vm.clearConsole,
				':ilist': function(_this) {
					var th = _this instanceof Array? _this[0]: _this;
					var _interps = th.getOptions().interpreters;
					if(_interps === undefined || _interps.length === 0) {
						return _msgLang["NO_INTERPS"];
					} else {
						return _interps.join('</br>');
					}
				},
				':ishow': function(_this) {
					var th = _this instanceof Array? _this.shift(): _this;
					return _msgLang["CURRENT_INTERP"].
						replace('@1', th.getOptions().currentInterpreter);
				},
				':iset': function(tokens) {
					var th = tokens instanceof Array? tokens.shift(): tokens;
					if(tokens === undefined || tokens.length === 0)
						return _msgLang["NO_PARAM_INTERP"];
					var interp = tokens.shift();
					if(!th.getOptions().interpreters.includes(interp))
						return _msgLang["NOT_FOUND_INTERP"].replace('@1', interp);
					th.setOptions({ currentInterpreter: interp });
					return _msgLang["CHANGE_INTERP"].
							replace('@1', th.getOptions().currentInterpreter);
				}
			};
		}
		/**
 		 * Renders the DOM elements necessary for the management of the console
 		 * @param parentElement the DOM element parent
 		 * @param opts configuration options
 		 **/
		function render(parentElement, opts) {
			_parentElement = parentElement;
			_echo = true;
			// Sets the options
     	if(opts.commandConsole !== undefined){
        $.extend(true, opts.commandConsole, defaultCommand());
      } else {
        opts.commandConsole = defaultCommand();
      }
      TutorConsole.setOptions(opts);
			// setting language
			changeLanguage();
			// Render template
			_parentElement.html( 
				'<div class="' + vm.getOptions().cssClass.content
				+ '" id="tcContent"></div>'
				+ '<div><form id="tcForm">'
				+ '<span class="' + vm.getOptions().cssClass.prompt
				+ '" id="tcPrompt">' + vm.getOptions().prompt + '&nbsp;</span>'
				+ '<label for="tcCmdLine" style="display:none;"></label>'
				+ '<input class="' + vm.getOptions().cssClass.cmdLine
				+ '" id="tcCmdLine" type="text">'
				+ '</form></div>');
    	_parentElement.addClass(vm.getOptions().cssClass.parent);
    	_parentElement.click( function() { $('#tcCmdLine').focus(); });
			// 
			vm.getForm().on('submit', function( e ) {
        submitForm(e);
        return false;
      });
      vm.getInput().on('keydown', function(e) {
        inputKeydown(e);
      });
		}
		/**
		 * Gets the options
		 **/
		function getOptions() {
			return TutorConsole.getOptions();
		}
		/**
		 * Set the new options
		 * @param opts the new options
		 **/
		function setOptions( opts ){
			if(opts.commandConsole !== undefined){
				$.extend(true, opts.commandConsole, defaultCommand());
			}
			TutorConsole.setOptions(opts);
			if(opts.prompt !== undefined) {
				vm.getPrompt().html(opts.prompt + '&nbsp;');
				vm.clearConsole(vm);
			}
			if(opts.language !== undefined) changeLanguage();
		}
		/**
		 * Gets the form DOM element
		 **/
		function getForm() {
			return angular.element('#tcForm');
		}
		/**
		 * Gets the input DOM element
		 **/
		function getInput() {
			return angular.element('#tcCmdLine');
		}
		/**
		 * Gets the content DOM element 
		 **/
		function getContent() {
			return angular.element('#tcContent');
		}
		/**
		 * Gets the prompt DOM element 
		 **/
		function getPrompt() {
			return angular.element('#tcPrompt');
		}
		/**
		 * Update the content element with the result of the execution of a
		 * command
		 * @param cmdLine command line
		 * @data result for execution of command line 
		 **/
		function updateContent(cmdLine, data) {
			let echo = '<span class="' + vm.getOptions().cssClass.prompt + '">'
				+ vm.getOptions().prompt + '&nbsp;</span>'
				+ '<span>' + cmdLine + '</span>';
			let html = '<div>'
				+ (_echo ? echo : "")
				+ '<div>' + ((data)? data: '') + '</div>'
				+ '</div>';
			vm.getContent().append(html);
			vm.reset();
		}
		/**
		 *
		 **/
		function reset() {
			var input = vm.getInput();
			input.prop('disabled', false);
			input.val('');
			input.focus();
			var pp = $(_parentElement).parent();
			if(pp !== null || pp !== undefined)
				$(pp).scrollTop($(pp)[0].scrollHeight);
			else
				$(_parentElement).scrollTop($(_parentElement)[0].scrollHeight);
		}
		/**
		 *
		 **/
		function hide() {
			vm.getInput().prop('disabled', true);
		}
		/**
		 * Clean console
		 **/
		function clearConsole(_this) {
			var th = _this instanceof Array? _this[0]: _this;
			th.getContent().html('');
			th.reset();
		}
		/**
		 * Execute the command typed in the console
		 **/
		function submitForm(event ) {
			_echo = true;
			var value = vm.getInput().val();
			var tokens = value.split(/\s+/);
			var cmd = tokens[0];

			if(!value) return;

			vm.hide();

			vm.execCommand(cmd, tokens, true);
  	}
		/**
		 * Execute command
		 * @param cmd command to execute
		 * @param tokens array of params
		 * @param echo true prints the executed prompt, false ignores it
		 **/
		function execCommand(cmd, tokens, echo) {
			_echo = echo === undefined ? true : echo;
			let t = cmd.startsWith(':') ? tokens.filter((e) => e !== cmd) : tokens;
			let value = cmd + " " + t.join(', ');

			//Execute command
			let result = undefined;
			if(Object.keys(defaultCommand()).includes(cmd)) {
				// if it is a default command, we passed the command and a reference to
				// the class
				t.unshift(vm);
				result = TutorConsole.execCommand(cmd, t);
			} else {
				result = TutorConsole.execCommand(cmd, t);
			}
			if(result === false) {
				let msg = _msgLang["NOT_FOUND_CMD"].replace('@1', cmd);
				vm.updateContent(value, msg);
				return;
			}

			if(result == undefined) return;

			if(cmd.indexOf('clear') != -1 || !result)
				return;

    	// Show result into content
    	vm.updateContent(value, result);
		}
		/**
		 *
		 **/
		function inputKeydown( e ) {
			var keycode = e.keyCode;
			var opts = vm.getOptions();
			switch(keycode) {
				case 9:  // Tab: command completion
					e.preventDefault();
					var cmds = [];
					var value = vm.getInput().val();
					if(value.match( /^[^\s]{0,}$/ ) ) {
						var keys = Object.keys(opts.commandConsole);
						for(var i in keys){
							if(value.trim() === '' || keys[i].indexOf(value) === 0)
								cmds.push(keys[i]);
						}
						if(opts.commandAllowed !== undefined) {
							var list = opts.commandAllowed.filter((e) =>
								!opts.commandNotAllowed.includes(e));
							for(var i in list)
								if(value.trim() === '' || i.indexOf(value) === 0)
									cmds.push(list[i]);
						}
						var keys = Object.keys(opts.macros);
						for(var i in keys){
							if(value.trim() === '' || keys[i].indexOf(value) === 0)
								cmds.push(keys[i]);
						}
					}
					if(cmds.length > 1) {
						vm.updateContent(value, cmds.join('</br>'));
						vm.getInput().val(value);
					} else if(cmds.length === 1)
						vm.getInput().val(cmds.pop() + ' ');
					else
						vm.getInput().val(value);
					break;
				case 38: // Up arrow
				case 40: // Down arrow
					e.preventDefault();
					var res = TutorConsole.showCommandHistory(keycode===38? true: false);
					vm.getInput().val(res? res: '');
					break;
				default:
					break;
			}
		}
		/**
 		 *
 		 **/
		function changeLanguage() {
			if(vm.getOptions().language === "es") _msgLang = ES
			else _msgLang = EN
		}
	}

	angular.module('tutorConsole')
		.service('TutorConsole', ['options', TutorConsole]);

	function TutorConsole(options) {
		var vm = this;
		vm.setOptions = setOptions;
		vm.getOptions = getOptions;
		vm.execCommand = execCommand;
		vm.showCommandHistory = showCommandHistory;

		activate();

		return vm;

		var hcurrent;
		var history;

		function activate() {
			hcurrent = null;
		 	history = [];
		}

		/**
 		 * Sets the console options
 		 * @param opts options of configuration
 		 **/
		function setOptions(opts) {
			if(!options) return;
		 	var opKeys = Object.keys(opts);
    	if(opKeys.length === 0) return;
    	for(var i=0; i<opKeys.length; i++){
      	options[opKeys[i]] = opts[opKeys[i]];
    	}
		}
		/**
 		 * Gets the console options
 		 **/
		function getOptions() {
			return options;
		}
		/**
 		 * Execute command
 		 * @param cmd command to execute
 		 * @param tokens params
 		 **/
		function execCommand(cmd, tokens) {
			// Validate command to execute
			if(!isValidCommand(cmd))
				return false;

			// Save in the history list
			if(vm.getOptions().history) {
				if(history.length > vm.getOptions().historyEntries)
					history.shift();
				history.push( cmd + ' ' +
					tokens.filter(function(el) {
						return el instanceof UITutorConsole? false: 
							el.trim() === cmd.trim()? false: true;
					}).join(' '));
				hcurrent = null;
			}

			// If is console command, executing the associated function and the
			// return value is returned by function.    
			if(Object.keys(vm.getOptions().commandConsole).includes(cmd)){
				var _fn = vm.getOptions().commandConsole[cmd];
				if(_fn === undefined)
					return undefined;
				return _fn(tokens);
			}

			// If the command is a macro, we substitute the value and execute
			// the command
			if(Object.keys(vm.getOptions().macros).includes(cmd)){
				var expanded = vm.getOptions().macros[cmd];
				for(var i=0; i<tokens.length; i++)
					expanded = expanded.replace('{'+i+'}', tokens[i]);
				tokens = expanded.split(/\s+/);
				cmd = tokens[0]
				tokens = tokens.filter((e)=>tokens.indexOf(e)>0);
			}

			if(!vm.getOptions().fnExecCommand)
				return false;

			return vm.getOptions().fnExecCommand(vm.getOptions().currentInterpreter,
				cmd, tokens);
		}
		/**
		 * Shows the history of executed commands
		 * @param isUp the arrow up key has been pressed
		 **/
		function showCommandHistory(isUp) {
			if(!history) return;
			var len = history.length;
			if(isUp){ // up arrow
				if(hcurrent === null) hcurrent = len;
				if(hcurrent < 0) return;
				hcurrent--;
			} else { // down arrow
				if(hcurrent === null || hcurrent === (len - 1)) return;
				hcurrent++;
			}
			return hcurrent >= 0 || hcurrent <= len?
				history[hcurrent]: null;
		}
		/**
		 *
		 **/
		function isValidCommand(cmd) {
			var pattern = /^:(.*)/

			if(!pattern.test(cmd)) return true;
			var match = cmd.match(pattern);

			// Validate commandConsole
			if(Object.keys(vm.getOptions().commandConsole).includes(match[0]))
				return true;
			// Validate commandNotAllowed
			if(vm.getOptions().commandNotAllowed && vm.getOptions().commandNotAllowed.includes(match[0]))
				return false;
			// Validate commandAllowed
			if(vm.getOptions().commandAllowed && vm.getOptions().commandAllowed.includes(match[0]))
				return true;
			// Validate macros
			if(Object.keys(vm.getOptions().macros).includes(match[0]))
				return true;
			return false;
		}
	}

	angular.module('tutorConsole')
		.directive('tcConsole', tcConsoleDirective);

	function tcConsoleDirective() {
		var directive = {
			restrict: 'E',
			replace: true,
			link: link
		};
		return directive;

		function link(scope, element, attrs) {
      var options = scope.$eval(attrs.options);
      var tc = scope.$eval(attrs.console);
      if(!tc instanceof UITutorConsole)
        throw new TypeError('The attribute "console" must be a instance of'
          + ' UITutorConsole.');
      tc.render(element.parent(), options);
		}
	}
